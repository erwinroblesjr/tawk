//
//  UserNote+CoreDataProperties.swift
//  Tawk
//
//  Created by Erwin Robles on 12/7/20.
//
//

import Foundation
import CoreData


extension UserNote {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserNote> {
        return NSFetchRequest<UserNote>(entityName: "UserNote")
    }

    @NSManaged public var userId: Int64
    @NSManaged public var note: String?

}

extension UserNote : Identifiable {

}
