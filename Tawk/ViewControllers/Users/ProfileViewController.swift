//
//  ProfileViewController.swift
//  Tawk
//
//  Created by Erwin Robles on 12/7/20.
//
protocol ProfileViewControllerDelegate {
    func updateNotesAdded()
}

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var userImage: CustomImageView!
    @IBOutlet weak var totalFollowersLabel: UILabel!
    @IBOutlet weak var totalFollowingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var blogLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var noteTextView: UITextView!
    
    var username: String?
    private var user: User?
    private var userNotes: [UserNote]?
    var delegate: ProfileViewControllerDelegate?
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        initializeViews()
        getProfile(userName: username ?? "")
    }
    
    @IBAction func onSaveTapped(_ sender: Any) {
        if userNotes?.filter({$0.userId == user?.id ?? -1}).count ?? 0 > 0{
            if let index = userNotes?.firstIndex(where: {$0.userId == user?.id ?? -1}){
                let userNote = userNotes![index]
                userNote.note = noteTextView.text
            }
            
        }else{
            let userNote = UserNote(context: self.context)
            userNote.userId = Int64(user?.id ?? -1)
            userNote.note = noteTextView.text
        }
        
        do {
            try self.context.save()
            delegate?.updateNotesAdded()
            self.showAlert(title: AlertDefaults.saveUserNote, message: AlertDefaults.saveUserNoteSuccess)
        } catch {
            self.showAlert(title: AlertDefaults.saveUserNote, message: AlertDefaults.saveUserNoteFailed)
        }
    }
    
    private func getProfile(userName: String){
        GetProfileRequest(delegate: self).send(userName: userName)
    }
    
    private func initializeViews(){
        navigationItem.title = username
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func updateViews(){
        if let url = URL(string: user?.avatar_url ?? ""){
            userImage.loadImage(with: url)
        }
        totalFollowersLabel.text = LabelDefaults.followers + " \(user?.followers ?? 0)"
        totalFollowingLabel.text = LabelDefaults.following + " \(user?.following ?? 0)"
        nameLabel.text = LabelDefaults.name + " \(user?.name ?? "")"
        companyLabel.text = LabelDefaults.company + " \(user?.company ?? "")"
        blogLabel.text = LabelDefaults.blog + " \(user?.blog ?? "")"
    }
    
    private func getNotes(){
        do {
            let userNotes: [UserNote] = try context.fetch(UserNote.fetchRequest()).filter({$0.userId == Int64(user?.id ?? -1)})
            self.userNotes = userNotes
            if userNotes.count > 0{
                noteTextView.text = userNotes[0].note
            }
        } catch {
            
        }
    }
    
}

extension ProfileViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("")
    }
}

extension ProfileViewController: GetProfileRequestDelegate{
    func getProfileSuccess(user: User) {
        self.user = user
        DispatchQueue.main.async {
            self.updateViews()
            self.getNotes()
        }
    }
    
    func getProfileFailed() {
        DispatchQueue.main.async {
//            self.loader.stopAnimating()
            self.showAlert(title: AlertDefaults.getProfileRequest, message: AlertDefaults.getProfileRequestFailed)
        }
    }
    
    
}
