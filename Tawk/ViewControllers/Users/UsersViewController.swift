//
//  UsersViewController.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

import UIKit

class UsersViewController: UIViewController{
    
    @IBOutlet weak var usersTableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var serchBar: UISearchBar!
    
    private var users: [User] = []
    private var usersHolder: [User] = []
    private var usersTemporaryHolder: [User] = []
    private var userNotes: [UserNote] = []
    private var loadingUsers = false
    private var tap: UITapGestureRecognizer?
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        initializeViews()
        getUsers(sinceId: 0)
    }
    
    private func getUsers(sinceId: Int){
        loadingUsers = true
        loader.startAnimating()
        GetUsersRequest(delegate: self).send(sinceId: sinceId)
    }
    
    private func initializeViews(){
        usersTableView.delegate = self
        usersTableView.dataSource = self
        usersTableView.register(UINib(nibName: NibDefaults.userTableViewCell, bundle: Bundle.main), forCellReuseIdentifier: IdentifierDefaults.userTableViewCellIdentifier)
        usersTableView.register(UINib(nibName: NibDefaults.userWithNoteTableViewCell, bundle: nil), forCellReuseIdentifier: IdentifierDefaults.userWithNoteTableViewCellIdentifier)
        tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        serchBar.delegate = self
    }
    
    private func getNotes(){
        do {
            userNotes = try context.fetch(UserNote.fetchRequest())
            for userNote in userNotes{
                for user in users{
                    if user.id ?? -1 == userNote.userId{
                        user.note = userNote.note
                    }
                }
            }
            
        } catch {
            
        }
    }
    
    @objc func dismissKeyboard() {
        if let tap = tap{
            view.removeGestureRecognizer(tap)
        }
        view.endEditing(true)
    }
}

extension UsersViewController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let profileViewController = segue.destination as? ProfileViewController, let userName = sender as? String{
            profileViewController.username = userName
            profileViewController.delegate = self
        }
    }
}

extension UsersViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (users[indexPath.row].note?.isEmpty ?? false || users[indexPath.row].note == nil){
            let cell = usersTableView.dequeueReusableCell(withIdentifier: IdentifierDefaults.userTableViewCellIdentifier, for: indexPath) as! UserTableViewCell
            cell.configure(user: users[indexPath.row], position: indexPath.row + 1)
            return cell
        }else{
            let cell = usersTableView.dequeueReusableCell(withIdentifier: IdentifierDefaults.userWithNoteTableViewCellIdentifier, for: indexPath) as! UserWithNoteTableViewCell
            cell.configure(user: users[indexPath.row], position: indexPath.row + 1)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let id = users[indexPath.row].id, indexPath.row + 1 == users.count, !loadingUsers, serchBar.text?.count == 0 {
            getUsers(sinceId: id)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: IdentifierDefaults.showProfileViewControllerIdentifier, sender: users[indexPath.row].login)
    }
    
}

extension UsersViewController: GetUsersRequestDelegate{
    func getUsersSuccess(users: [User]) {
        loadingUsers = false
        self.users.append(contentsOf: users)
        self.usersHolder.append(contentsOf: users)
        getNotes()
        DispatchQueue.main.async {
            self.usersTableView.reloadData()
            self.loader.stopAnimating()
        }
    }
    
    func getUsersFailed() {
        loadingUsers = false
        DispatchQueue.main.async {
            self.loader.stopAnimating()
            self.showAlert(title: AlertDefaults.getUsersRequest, message: AlertDefaults.getUsersRequestFailed)
        }
    }
    
}

extension UsersViewController: UISearchBarDelegate{
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if let tap = tap{
            view.addGestureRecognizer(tap)
        }
        return true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            users.removeAll()
            users.append(contentsOf: usersHolder)
        }else{
            let filteredUsers = usersHolder.filter({($0.login?.uppercased().contains(searchText.uppercased()) ?? false) || ($0.login?.uppercased().contains(searchText.uppercased()) ?? false)})
            users.removeAll()
            users.append(contentsOf: filteredUsers)
        }
        usersTableView.reloadData()
    }
}

extension UsersViewController: ProfileViewControllerDelegate{
    func updateNotesAdded() {
        getNotes()
        usersTableView.reloadData()
    }
}
