//
//  ApiRequest.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

import Foundation

protocol ApiRequestDelegate {
    func success(data: Data)
    func failed()
}

class ApiRequest{
    
    var delegate: ApiRequestDelegate?
    
    init(delegate: ApiRequestDelegate) {
        self.delegate = delegate
    }
    
    func send(url: String){
        guard let url = URL(string: url) else { return }
        URLSession.shared.dataTask(with: url){(data, response, err) in
            if let data = data{
                if let httpResponse = response as? HTTPURLResponse {
                    switch (httpResponse.statusCode) {
                    case 200, 201, 202:
                        self.delegate?.success(data: data)
                        //                            let decodedResponse = try JSONDecoder().decode(T.Response.self, from: data)
                        //                            completion(.success(decodedResponse))
                        break;
                    default:
                        self.delegate?.failed()
                        //                            let decodedResponse = try JSONDecoder().decode(ErrorResponse.self, from: data)
                        //                            completion(.responseError(decodedResponse))
                        break;
                    }
                }
            }else{
                self.delegate?.failed()
                //                completion(.failure("Request Error!"))
            }
        }.resume()
    }
    
}
