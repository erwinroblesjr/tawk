//
//  GetProfileRequest.swift
//  Tawk
//
//  Created by Erwin Robles on 12/7/20.
//

import Foundation

protocol GetProfileRequestDelegate {
    func getProfileSuccess(user: User)
    func getProfileFailed()
}

class GetProfileRequest{
    
    var delegate: GetProfileRequestDelegate?
    
    init(delegate: GetProfileRequestDelegate) {
        self.delegate = delegate
    }
    
    func send(userName: String){
        let url = UrlDefaults.baseUrl + UrlDefaults.getProfile(userName: userName)
        ApiRequest(delegate: self).send(url: url)
    }
}

extension GetProfileRequest: ApiRequestDelegate{
    func success(data: Data) {
        do{
            let decodedResponse = try JSONDecoder().decode(User.self, from: data)
            delegate?.getProfileSuccess(user: decodedResponse)
        }catch _{
            delegate?.getProfileFailed()
        }
    }
    
    func failed() {
        delegate?.getProfileFailed()
    }
    
    
}
