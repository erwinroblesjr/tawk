//
//  ApiManager.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//
import Foundation

protocol GetUsersRequestDelegate {
    func getUsersSuccess(users: [User])
    func getUsersFailed()
}

class GetUsersRequest{
    
    var delegate: GetUsersRequestDelegate?
    
    init(delegate: GetUsersRequestDelegate) {
        self.delegate = delegate
    }
    
    func send(sinceId: Int){
        let url = UrlDefaults.baseUrl + UrlDefaults.getUsers(sinceId: sinceId)
        ApiRequest(delegate: self).send(url: url)
    }
}

extension GetUsersRequest: ApiRequestDelegate{
    func success(data: Data) {
        do{
            let decodedResponse = try JSONDecoder().decode([User].self, from: data)
            delegate?.getUsersSuccess(users: decodedResponse)
        }catch _{
            delegate?.getUsersFailed()
        }
    }
    
    func failed() {
        delegate?.getUsersFailed()
    }
    
    
}
