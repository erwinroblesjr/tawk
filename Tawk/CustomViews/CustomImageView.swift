//
//  CustomImageView.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

import UIKit

let imageCahched = NSCache<AnyObject, AnyObject>()

class CustomImageView: UIImageView{
    
   var task: URLSessionDataTask!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadImage(with url: URL, position: Int? = -1){
        
        
        if let task = task{
            task.cancel()
        }
        
        if let imageCache = imageCahched.object(forKey: url.absoluteString as AnyObject) as? UIImage{
            self.image = imageCache.invert(position: position)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error{
                print("loadImage", error)
                return
            }
            let imageToCache = UIImage(data: data!)
            imageCahched.setObject(imageToCache!, forKey: url.absoluteString as AnyObject)
            DispatchQueue.main.async {
                self.image = imageToCache?.invert(position: position)
            }
        }.resume()
    }
}
