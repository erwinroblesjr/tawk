//
//  UserTableViewCell.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//
import UIKit

class UserWithNoteTableViewCell: UITableViewCell {
    @IBOutlet weak var userImage: CustomImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    
    func configure(user: User, position: Int){
        usernameLabel.text = user.login
        detailsLabel.text = "\(user.type ?? "")"
        if let url = URL(string: user.avatar_url ?? ""){
            userImage.loadImage(with: url, position: position)
        }
    }
}
