//
//  UrlDefaults.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

class UrlDefaults{
    static let baseUrl = "https://api.github.com"
    static func getUsers(sinceId: Int) -> String{
        return "/users?since=\(sinceId)"
    }
    static func getProfile(userName: String) -> String{
        return "/users/\(userName)"
    }
    
}
