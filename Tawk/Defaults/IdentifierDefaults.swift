//
//  IdentifierDefaults.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

class IdentifierDefaults{
    static let userTableViewCellIdentifier = "userTableViewCellIdentifier"
    static let userWithNoteTableViewCellIdentifier = "userWithNoteTableViewCellIdentifier"
    static let showProfileViewControllerIdentifier = "showProfileViewControllerIdentifier"
}
