//
//  AlerDefaults.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

class AlertDefaults{
    
    static let getUsersRequest = "Get Users Request"
    static let getUsersRequestFailed = "Get Users Request Failed!"
    static let oK = "OK"
    static let getProfileRequest = "Get Profile Request"
    static let getProfileRequestFailed = "Get Profile Request Failed!"
    static let saveUserNote = "Save User Note"
    static let saveUserNoteFailed = "Save User Note Failed!"
    static let saveUserNoteSuccess = "Save User Note Success!"
    
}
