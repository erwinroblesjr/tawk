//
//  LabelDefaults.swift
//  Tawk
//
//  Created by Erwin Robles on 12/7/20.
//

class LabelDefaults{
    
    static let followers = "followers:"
    static let following = "following:"
    static let name = "name:"
    static let company = "company:"
    static let blog = "blog:"
}
