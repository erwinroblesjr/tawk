//
//  Defaults.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

class NibDefaults{
    static let userTableViewCell = "User"
    static let userWithNoteTableViewCell = "UserWithNote"
}
