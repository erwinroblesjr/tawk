//
//  User.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//

class User: Decodable{
    
    let id: Int?
    let login: String?
    let avatar_url: String?
    let hasNote: Bool?
    let followers: Int?
    let following: Int?
    let name: String?
    let company: String?
    let blog: String?
    let type: String?
    var note: String?
}
