//
//  UIImageView+Ext.swift
//  Tawk
//
//  Created by Erwin Robles on 12/7/20.
//

import UIKit

extension UIImage{
    
    func invert(position: Int?) -> UIImage{
        if let position = position, position % 4 != 0{ return self }
        let filter = CIFilter(name: "CIColorInvert")
        filter?.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        guard let outputImage = filter?.outputImage else { return self }
        return UIImage(ciImage: outputImage)
    }
}
