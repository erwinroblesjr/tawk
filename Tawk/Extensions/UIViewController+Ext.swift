//
//  UIViewController+Ext.swift
//  Tawk
//
//  Created by Erwin Robles on 12/6/20.
//
import UIKit

extension UIViewController{
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: AlertDefaults.oK, style: .default, handler: { action in
        }))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }
}
